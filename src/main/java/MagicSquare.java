import java.util.*;

public class MagicSquare {

    public static boolean isMagicSquare(int[][] arr){
        int row_sum, col_sum, diag1_sum=0, diag2_sum=0;
        int size= arr.length;
        for(int i=0; i<size; i++){
            diag1_sum += arr[i][i];
            diag2_sum += arr[i][size-i-1];
        }
        if(diag1_sum!=diag2_sum)
            return false;
        for(int row=0; row<size; row++){
            row_sum=0;
            col_sum=0;
            for(int col=0; col<size; col++){
                row_sum += arr[row][col];
                col_sum += arr[col][row];
            }
            if(row_sum!=diag1_sum || col_sum!=diag2_sum)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int size;
        size= sc.nextInt();
        int[][] arr= new int[size][size];
        for(int row=0; row<size; row++){
            for(int column=0; column<size; column++){
                arr[row][column]= sc.nextInt();
            }
        }
        boolean is_magic_square= isMagicSquare(arr);
        if(is_magic_square)
            System.out.println("yes");
        else
            System.out.println("no");
    }
}
